import Vue from 'vue'
import Router from 'vue-router'
import AddPayment from '@/pages/AddPayment'
import NextPayer from '@/pages/NextPayer'
import PaymentList from '@/pages/PaymentList'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'NextPayer',
      component: NextPayer
    },
    {
      path: '/addpayment',
      name: 'AddPayment',
      component: AddPayment
    },
    {
      path: '/paymentlist',
      name: 'PaymentList',
      component: PaymentList
    }
  ]
})
